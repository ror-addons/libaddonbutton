LibAddonButton.Menu = {};

local DEFAULT_WIDTH = 1000;
local DEFAULT_HEIGHT = 28;
local DEFAULT_TOOLTIP_WIDTH = 300;

local menuItems = {};
local menuData = {};
local tooltip = nil;

function LibAddonButton.Menu.Create(menuId)
	menuData[menuId] = {};
end

function LibAddonButton.Menu.AddMenuItem(menuId, buttonText, callbackFunction, tooltip)

	local contextMenu = EA_Window_ContextMenu.contextMenus[menuId] or {};
	local index = (contextMenu.numUserDefinedMenuItems or 0) + 1;
	
	local menuName = "LibAddonButtonMenu" .. menuId .. "Item" .. index;

	if (menuItems[menuName] == nil) then
		menuItems[menuName] = true;
        CreateWindowFromTemplate(menuName, "LibAddonButtonMenuItemTemplate", "Root");
	end
	
	local contextMenu = EA_Window_ContextMenu.contextMenus[menuId] or {};
	local menuIndex = (contextMenu.numUserDefinedMenuItems or 0) + 1;
    
	WindowSetDimensions(menuName, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	ButtonSetText(menuName, buttonText);
	
	local width = ButtonGetTextDimensions(menuName);
	width = math.max(width + EA_Window_ContextMenu.BUTTON_TEXT_OFFSET * 2, EA_Window_ContextMenu.MIN_WIDTH);
	
	WindowSetDimensions(menuName, width, DEFAULT_HEIGHT);
	
	menuData[menuId][menuIndex] =
	{
		Index = index,
		Window = menuName,
		Width = width,
		Callback = callbackFunction,
		Tooltip = tooltip,
	};
	
	EA_Window_ContextMenu.AddUserDefinedMenuItem(menuName, menuId);

end

function LibAddonButton.Menu.Finalize(menuId)

	local contextMenu = EA_Window_ContextMenu.contextMenus[menuId] or {};
	local greatestWidth = math.max(contextMenu.greatestWidth or 0, EA_Window_ContextMenu.MIN_WIDTH);

	for index, menu in pairs(menuData[menuId]) do
		if (menu.Width < greatestWidth) then
			menu.Width = greatestWidth;
			WindowSetDimensions(menu.Window, greatestWidth, DEFAULT_HEIGHT);
		end
	end
	
end

function LibAddonButton.Menu.OnLButtonUp()

	local mouseOverWindow = SystemData.MouseOverWindow.name;
    
	local menuIndex = WindowGetId(mouseOverWindow);
	local menuId = WindowGetId(WindowGetParent(mouseOverWindow));
	
	if (menuData[menuId] and menuData[menuId][menuIndex]) then
		local menu = menuData[menuId][menuIndex];
		
		if (type(menu.Callback) == "function") then
			menu.Callback();
		end
		EA_Window_ContextMenu.HideAll();
	end
	
end

local function ShowTooltip(menuName, tooltipText)

	if (not tooltip) then
		tooltip = 
		{
			Window = "LibAddonButtonMenuItemTooltip",
			IsActive = false,
		};
		CreateWindowFromTemplate(tooltip.Window, "LibAddonButtonMenuItemTooltipTemplate", "Root");
	end

	WindowSetDimensions(tooltip.Window .. "Text", DEFAULT_TOOLTIP_WIDTH, 1000);
	LabelSetText(tooltip.Window .. "Text", tooltipText);
	local width, height = LabelGetTextDimensions(tooltip.Window .. "Text");
	WindowSetDimensions(tooltip.Window .. "Text", width, height);
	WindowSetDimensions(tooltip.Window, width + 10, height + 10);
	
	WindowClearAnchors(tooltip.Window);
	WindowAddAnchor(tooltip.Window, "topright", menuName, "topleft", 15, 0);
	WindowSetShowing(tooltip.Window, true);
	
	tooltip.IsActive = true;
	
end

local function HideTooltip()

	if (tooltip and tooltip.IsActive) then
		WindowSetShowing(tooltip.Window, false);
		tooltip.IsActive = false;
	end

end

function LibAddonButton.Menu.OnMouseOver()

	-- for hiding cascading context menus
	EA_Window_ContextMenu.OnMouseOverDefaultMenuItem();

	local mouseOverWindow = SystemData.MouseOverWindow.name;
    
	local menuIndex = WindowGetId(mouseOverWindow);
	local menuId = WindowGetId(WindowGetParent(mouseOverWindow));
	
	if (menuData[menuId] and menuData[menuId][menuIndex]) then
		local menu = menuData[menuId][menuIndex];
		if (menu.Tooltip and menu.Tooltip:len() > 0) then
		
			ShowTooltip(mouseOverWindow, menu.Tooltip);
		
		end
	end

end

function LibAddonButton.Menu.OnMouseOut()

	HideTooltip();

end