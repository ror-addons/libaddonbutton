--[[

Registering a button:
	LibAddonButton.Register("uniqeName", buttonWidth, buttonHeight, "DefaultTexture", "HighlightTexture", "PressedTexture", autoSort)
	
	By default, if the "PressedTexture" or "HighlightTexture" is left blank, then "DefaultTexture" will be used. If they
	are all left blank, the default animated gear button will be used.
	
	autoSort defines whether the menu should be automatically sorted alphabetically or not.
	
	A texture can be a simple string that is the name of a texture, or an animation. The format of an animation is:
	
	local animation =
	{
		FPS = 10,	-- display 1 frame every 0.1 seconds
		Frames =	-- this must be an index-based array from 1 to n
		{
			"FrameTexture1", "FrameTexture2", "FrameTexture3"
		}
	}
	
Adding a menu item:
	LibAddonButton.AddMenuItem("uniqeName", L"label text", callbackFunction)
	
Adding a user defined menu item:
	LibAddonButton.AddUserDefinedMenuItem("uniqueName", L"label text", window)
	
	For user defined menu items, the label text is only used for sorting and in the manager.
	
Adding a menu divider:
	LibAddonButton.AddMenuDivider("uniqeName")
	
Adding a cascading menu item:
	local menu = LibAddonButton.AddCascadingMenuItem("uniqeName", L"label text", autoSort)
	
	This function returns the created menu that is used to add submenu items.
	Examples to add submenu items:
	
	LibAddonButton.AddMenuSubitem(menu, L"A", callbackFunction)
	menu = LibAddonButton.AddCascadingMenuSubitem(menu, L"B", callbackFunction)
	LibAddonButton.AddMenuSubitem(menu, L"C", callbackFunction)
	menu = LibAddonButton.AddCascadingMenuSubitem(menu, L"D", callbackFunction)
	LibAddonButton.AddMenuSubitem(menu, L"E", callbackFunction)
	LibAddonButton.AddMenuDividerSubitem(menu);
	LibAddonButton.AddUserDefinedMenuSubitem(menu, L"F", window)
	
	The above will result in:
	
	A
	B	>	C
			D	>	E | F
			
Overriding the default behavior of the LUp/RUp events:
	First you must find the button using either method:
	
	local button = LibAddonButton.Register("uniqueName", ... )
	local button = LibAddonButton.GetButton("uniqueName")
	
	Then to override the LUp/RUp events:
	
	button:SetLUpOverride(callbackFunction)
	button:SetRUpOverride(callbackFunction)
	
	The following parameters will be passed to the callback function:
	
	flags, mouseX, mouseY
	
Interrupting menu creation:
	A callback can be set in the same way overrides are. The callback
	will be called just before a menu is finalized (shown), allowing for
	core menu modifications.
	
	To set the override:
	
	button:SetFinalizeMenuOverride(callbackFunction)
	
	The following parameters will be passed to the callback function:
	
	menuId
	
	If true is returned by the callback function, the menu will be aborted
	and finalize will not be called.
	
--]]

LibAddonButton = LibAddonButton or {};
LibAddonButton.RegisteredButtons = {};
LibAddonButton.CreatedButtons = {};

local VERSION_SETTINGS = 2;
local CREATED_PREFIX = ".Created.";
local DELAY_MACRO = 0;

local MacroCommand =
{
	Delay = L"delay",
};

local refreshRequired = false;
local updateQueue = {};
local registeredButtons = LibAddonButton.RegisteredButtons;
local animatedButtons = {};
local animatedButtonList = {};
local animatedButtonCount = 0;

local macroQueue = {};
local macroReady = false;
local nextMacro = 0;
local macroDelay = DELAY_MACRO;

local function AddAnimation(name)

	if (not animatedButtons[name]) then
		animatedButtonCount = animatedButtonCount + 1;
		table.insert(animatedButtonList, registeredButtons[name].Button);
	end

	animatedButtons[name] = true;

end

local function RecacheAnimatedButtons()

	animatedButtonList = {};
	for name, _ in pairs(animatedButtons) do
		table.insert(animatedButtonList, registeredButtons[name].Button);
	end

end

local function QueueUpdate(name)
	updateQueue[name] = true;
	refreshRequired = true;
end

local function UpdateButton(name)

	local button = registeredButtons[name];
	if (not button) then return end
	
	--[[ The original intent was to hide buttons with no menu items
	local menuItems = #button.Menu.Subitems;
	button:Show(menuItems > 0);
	--]]
	
	
	local settings = LibAddonButton.Settings.Buttons[name];
	if (settings and settings.IsShowing == false) then
		button.Button:Show(false);
	else
		button.Button:Show(true);
	end

end

local function LoadCreatedButtons()

	local createdButtons = LibAddonButton.Settings.CreatedButtons;
	
	for name, createdButton in pairs(createdButtons) do	
	
		if (#createdButton.Menu.Subitems == 0) then
			-- this item has been loaded with no purpose, delete it
			LibAddonButton.Settings.CreatedButtons[name] = nil;
			LibAddonButton.Settings.Buttons[name] = nil;	
		else
			local button = LibAddonButton.Register(name, createdButton.Size.Width, createdButton.Size.Height, createdButton.Icons.Default, createdButton.Icons.Highlight, createdButton.Icons.Pressed);
			button.CreatedByUser = true;
			
			LibAddonButton.CreatedButtons[name] = button;
		end
	end

end

local function UpdateSettings()

	local settings = LibAddonButton.Settings;
	local version = settings.Version;
	
	if (not version) then
		version = 1;
		LibAddonButton.Settings = { Buttons = {} };
		for id, button in pairs(settings) do
			LibAddonButton.Settings.Buttons[id] = button;
		end
		settings = LibAddonButton.Settings;
	end
	
	if (version == 1) then
		version = 2;
		if (settings.CreatedButtons) then
			for name, createdButton in pairs(settings.CreatedButtons) do
			
				createdButton.Menu = { AutoSort = true, Subitems = createdButton.MenuItems };
				createdButton.MenuItems = nil;
				
				for index, menuItem in pairs(createdButton.Menu.Subitems) do
					if (menuItem.Macro) then
						menuItem.Created = true;
					end
				end
				
			end
		end
	end

	if (version == 2) then
		version = 3;
	end
	
end

local function LoadSettings()

	if (not LibAddonButton.Settings) then
		LibAddonButton.Settings = {};
	else
		UpdateSettings();
	end
	
	local settings = LibAddonButton.Settings;
	settings.Version = VERSION_SETTINGS;
	
	if (not settings.Buttons) then
		settings.Buttons = {};
	end
	if (not settings.CreatedButtons) then
		settings.CreatedButtons = {};
	end
	
end

function LibAddonButton.Initialize()

	LibAddonButton.Manager.Initialize();
	
	LoadSettings();
	LoadCreatedButtons();
	
end

function LibAddonButton.Unload()

	LibAddonButton.SaveButtons();

end

function LibAddonButton.SaveButtons()

	for name, registeredButton in pairs(registeredButtons) do
		LibAddonButton.SaveButton(registeredButton);
	end
	
end

function LibAddonButton.SaveButton(registeredButton)

	local button = registeredButton.Button;

	if (button and DoesWindowExist(button:GetName())) then
		local buttonScale = button:GetScale();
		local buttonAlpha = WindowGetAlpha(button:GetName());
		local interfaceScale = InterfaceCore.GetScale();
		local isShowing = WindowGetShowing(button:GetName());
		local x, y = WindowGetScreenPosition(button:GetName());
		if (x and y) then
			local buttonSettings = LibAddonButton.Settings.Buttons;
			buttonSettings[button.Name] = 
			{ 
				X = x / interfaceScale, 
				Y = y / interfaceScale, 
				IsShowing = isShowing, 
				Scale = buttonScale, 
				Alpha = buttonAlpha,
			};
		end
	end

end

function LibAddonButton.CreateFromTemplate(template)

	local name = CREATED_PREFIX;
	local index = 1;

	while (LibAddonButton.CreatedButtons[name .. index]) do
		index = index + 1;
	end
	
	local settings = LibAddonButton.Settings;
	settings.CreatedButtons[name .. index] =
	{
		Size = template.Size,
		Icons = template.Icons,
		Menu = 
		{
			AutoSort = true,
			Subitems = {},
		},
	};

	local button = LibAddonButton.Register(name .. index, template.Size.Width, template.Size.Height, template.Icons.Default, template.Icons.Highlight, template.Icons.Pressed);
	button.CreatedByUser = true;
	
	LibAddonButton.CreatedButtons[name .. index] = button;

	return button;

end

function LibAddonButton.Remove(name)

	if (not registeredButtons[name]) then return end
	
	local button = registeredButtons[name].Button;
	LayoutEditor.UnregisterWindow(button:GetName());
	
	if (animatedButtons[name]) then
		animatedButtonCount = animatedButtonCount - 1;
		animatedButtons[name] = nil;
	end
	
	LibAddonButton.Settings.CreatedButtons[name] = nil;
	LibAddonButton.Settings.Buttons[name] = nil;
	LibAddonButton.CreatedButtons[name] = nil;
	LibAddonButton.RegisteredButtons[name] = nil;
	button:Destroy();
	
end

function LibAddonButton.Register(name, width, height, defaultIcon, highlightIcon, pressedIcon, autoSort)

	if (autoSort == nil) then
		autoSort = true;
	end

	-- if called with no name, one will be generated
	if (not name) then
		name = 1;
		while (registeredButtons[name]) do
			name = name + 1;
		end
	end

	if (not registeredButtons[name]) then
		registeredButtons[name] = 
		{	
			Button = LibAddonButton.Button:Create(name, width, height, defaultIcon, highlightIcon, pressedIcon),
			Menu = { AutoSort = autoSort, Subitems = {} },
			Name = name,
		}
		
		if (type(defaultIcon) == "table" or type(highlightIcon) == "table" or type(pressedIcon) == "table" or not (defaultIcon and highlightIcon and pressedIcon)) then
			AddAnimation(name);
		end
		
		local button = registeredButtons[name].Button;		
		local settings = LibAddonButton.Settings.Buttons[name];
		
		if (settings) then
			button:SetPosition(settings.X, settings.Y);
			if (settings.Scale) then
				button:SetScale(settings.Scale);
			end
			if (settings.Alpha) then
				button:SetAlpha(settings.Alpha);
			end
		else
			button:ToggleLock(true); -- the first time a button is seen, it is automatically unlocked
		end
		
		LayoutEditor.RegisterWindow(button:GetName(), L"", towstring(name), false, false, true);
		
		QueueUpdate(name);
	end
	
	return registeredButtons[name].Button;

end

function LibAddonButton.UpdateButtonIsAnimated(name, isAnimated)

	if (isAnimated) then
		AddAnimation(name);
	else
		if (animatedButtons[name]) then
			animatedButtonCount = animatedButtonCount - 1;
			animatedButtons[name] = nil;
			RecacheAnimatedButtons();
		end
	end
		
end

local function InsertMenuItem(menu, menuItem)

	table.insert(menu.Subitems, menuItem);
	
	local item = menu;
	while (item.Parent) do
		item = item.Parent;
	end
	if (item.Name) then
		QueueUpdate(item.Name);
	end
	
	return menuItem;
	
end

local function AddUserDefinedMenuItem(menu, text, window)

	local menuItem =
	{
		Text = text,
		UserDefined = window,
	};
	
	return InsertMenuItem(menu, menuItem);
	
end

local function AddMenuItem(menu, text, isCascading, callbackFunction, isDivider, tooltip)
	
	local menuItem =
	{
		Text = towstring(text or ""),
		Callback = callbackFunction,
	};
	
	if (not isCascading and not isDivider and tooltip) then
		menuItem.Tooltip = towstring(tooltip);
	end
	
	if (isCascading) then
		menuItem.Parent = menu;
		menuItem.Subitems = {};
	end
	
	if (isDivider) then
		menuItem = { Divider = true };
	end
	
	return InsertMenuItem(menu, menuItem);

end

function LibAddonButton.AddMenuItem(name, text, callbackFunction, tooltip)

	if (not registeredButtons[name]) then return end
	local registeredButton = registeredButtons[name];
	text = towstring(text);
	
	AddMenuItem(registeredButton.Menu, text, false, callbackFunction, false, tooltip);

end

function LibAddonButton.AddUserDefinedMenuItem(name, text, window)

	if (not registeredButtons[name]) then return end
	local registeredButton = registeredButtons[name];
	text = towstring(text);
	
	AddUserDefinedMenuItem(registeredButton.Menu, text, window);
	
end

function LibAddonButton.AddMenuDivider(name)

	if (not registeredButtons[name]) then return end
	local registeredButton = registeredButtons[name];
	
	AddMenuItem(registeredButton.Menu, nil, nil, nil, true);

end

function LibAddonButton.AddCascadingMenuItem(name, text, autoSort)

	if (not registeredButtons[name]) then return end
	local registeredButton = registeredButtons[name];
	text = towstring(text);
	
	local menu = AddMenuItem(registeredButton.Menu, text, true);
	menu.AutoSort = autoSort;
	
	return menu;

end

function LibAddonButton.AddMenuSubitem(menu, text, callbackFunction, tooltip)

	if (not menu or not menu.Subitems) then return end
	AddMenuItem(menu, text, false, callbackFunction, false, tooltip);

end

function LibAddonButton.AddUserDefinedMenuSubitem(menu, text, window)

	if (not menu or not menu.Subitems) then return end
	AddUserDefinedMenuItem(menu, text, window);

end

function LibAddonButton.AddMenuDividerSubitem(menu)

	if (not menu or not menu.Subitems) then return end
	AddMenuItem(menu, nil, nil, nil, true);

end

function LibAddonButton.AddCascadingMenuSubitem(menu, text, autoSort)

	if (not menu or not menu.Subitems) then return end
	
	local menu = AddMenuItem(menu, text, true);
	menu.AutoSort = autoSort;
	
	return menu;
	
end

-- Returns the button for the specified name
function LibAddonButton.GetButton(name)

	if (not registeredButtons[name]) then return end
	return registeredButtons[name].Button;

end

local function IsMacroCommand(macro)

	local firstChar = macro:sub(1, 1);
	
	if (firstChar == L"#") then
		local command, value = macro:match(L"^#([a-z0-9]+)[ ]?(.*)");
		command = command:lower();
		
		if (command == MacroCommand.Delay) then
			return command, value;
		end
	end

end

-- runs a queued macro
local function RunMacro(macro)

	if (not macro) then return end
	local firstChar = macro:sub(1, 1);

	if (firstChar == L"#") then
		local command, value = macro:match(L"^#([a-z0-9]+)[ ]?(.*)");
		command = command:lower();
		
		if (command == MacroCommand.Delay) then
			if (value) then
				macroDelay = tonumber(value);
			end
			return;
		end
	end
	
	if (LibSlash and firstChar == L"/") then
		local command, args = macro:match(L"^/([a-z0-9\x0430-\x044F\x0451\x0410-\x042F\x0401]+)[ ]?(.*)");
		if (LibSlash.IsSlashCmdRegistered(command)) then
			slash(macro:sub(2));
			return;
		end
	end

	SendChatText(macro, L"");

end

local function RunNextMacro()
	
	-- keep looping while the next command is a macro command ex "#delay"
	while (#macroQueue > 0 and IsMacroCommand(macroQueue[1])) do
		RunMacro(table.remove(macroQueue, 1));
		macroReady = (#macroQueue > 0);
		if (not macroReady) then return end
	end
	
	RunMacro(table.remove(macroQueue, 1));
	macroReady = (#macroQueue > 0);
			
end

-- adds a macro to the queue
function LibAddonButton.RunMacro(macro)

	macroReady = true;
	
	local delayAdded = false;
	local macroLines = WStringSplit(macro, L"\r");
	
	for _, macroLine in ipairs(macroLines) do
		if (macroLine:len() > 0) then
			local command, value = IsMacroCommand(macroLine);
			if (command == MacroCommand.Delay) then
				delayAdded = true;
			end
			table.insert(macroQueue, macroLine);
		end
	end
	
	if (delayAdded) then
		table.insert(macroQueue, L"#" .. MacroCommand.Delay .. L" " .. towstring(DELAY_MACRO));
	end

end

function LibAddonButton.OnUpdate(elapsed)

	if (macroReady) then
		if (nextMacro <= 0) then
			RunNextMacro();
			nextMacro = macroDelay;
		else
			nextMacro = nextMacro - elapsed;
		end
	end

	if (refreshRequired) then
		refreshRequired = false;
		for name, _ in pairs(updateQueue) do
			UpdateButton(name);
		end
		updateQueue = {};
	end
	
	if (animatedButtonCount > 0) then
		for _, button in ipairs(animatedButtonList) do
			button:Update(elapsed);
		end
	end
	
	LibAddonButton.Manager.OnUpdate(elapsed);

end